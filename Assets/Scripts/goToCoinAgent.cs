﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine.UI;

public class goToCoinAgent : Agent
{
    public Transform[] target;
    public int Spd;
    private Vector3 initPosition;
    private Vector3 lastPosition;
    private int counterI = 0;
    private int counter2 = 0;
    public Text counter;
    private List<GameObject> coins = new List<GameObject>();
    public GameObject lastCoin;
    //mlagents-learn --run-id=Test
    //mlagents-learn config\config.yaml --run-id=lab1 --resume



    private void Start()
    {
        initPosition = this.transform.localPosition;
        lastPosition = this.transform.localPosition;
    }

    public override void OnEpisodeBegin()
    {
        this.transform.localPosition = initPosition;
        counter2 = 0;
        if (coins != null) {
            foreach (GameObject coin in coins)
            {
                coin.SetActive(true);
            }
        }
        lastCoin.SetActive(true);

    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.localPosition);
        sensor.AddObservation(target[counter2].localPosition);

    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        float moveX = actions.ContinuousActions[0];
        float moveY = actions.ContinuousActions[1];

        lastPosition = this.transform.localPosition;
        transform.position += new Vector3(moveX, moveY, this.transform.position.z) * Time.deltaTime * Spd;

        if (Vector3.Distance(this.transform.localPosition, target[counter2].localPosition) > Vector3.Distance(lastPosition, target[counter2].localPosition))
        {
            AddReward(0.1f);
        }
        else if (Vector3.Distance(this.transform.localPosition, target[counter2].localPosition) <= Vector3.Distance(lastPosition, target[counter2].localPosition))
        {
            AddReward(-0.2f);
        }
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> continousActions = actionsOut.ContinuousActions;
        continousActions[0] = Input.GetAxisRaw("Horizontal");
        continousActions[1] = Input.GetAxisRaw("Vertical");
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //SetReward para 1 recompensa
        //AddReward para hacer incrementos de recompensas
        if (other.gameObject.tag == "wall")
        {
            AddReward(-30f);
            EndEpisode();
        }
        else if (other.gameObject.tag == "lastcoin")
        {
            AddReward(30f);
            counterI++;
            counter.text = counterI.ToString();
            other.gameObject.SetActive(false);
            EndEpisode();
        }
        else if (other.gameObject.tag == "coin")
        {
            AddReward(30f);
            counterI++;
            counter2 = 1;
            counter.text = counterI.ToString();
            coins.Add(other.gameObject);
            other.gameObject.SetActive(false);
        }
        else if (other.gameObject.tag == "coin2")
        {
            AddReward(30f);
            counterI++;
            counter2 = 2;
            counter.text = counterI.ToString();
            coins.Add(other.gameObject);
            other.gameObject.SetActive(false);
        }
        else if (other.gameObject.tag == "coin3")
        {
            AddReward(30f);
            counterI++;
            counter2 = 3;
            counter.text = counterI.ToString();
            coins.Add(other.gameObject);
            other.gameObject.SetActive(false);
        }
    }
}
